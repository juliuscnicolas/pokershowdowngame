﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokerHandShowdownWinApp
{
    public partial class PokerTable : Form 
    {
        private Card[] playerOneHand;
        private Card[] playerTwoHand;
        private Card[] playerThreeHand;
        private Card[] sortedplayerOneHand;
        private Card[] sortedplayerTwoHand;
        private Card[] sortedplayerThreeHand;

        DeckOfCards dc = new DeckOfCards();
        Bitmap cardsuitImage = Properties.Resources.club;

        public PokerTable()
        {
            InitializeComponent();

            playerOneHand = new Card[5];
            sortedplayerOneHand = new Card[5];
            playerTwoHand = new Card[5];
            sortedplayerTwoHand = new Card[5];
            playerThreeHand = new Card[5];
            sortedplayerThreeHand = new Card[5];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Deal();   
        }

        public void Deal()
        {
          dc.setUpDeck(); 
          getHand();
          sortCards();
          displayCards();
          evaluateHands();
        }

        public void getHand()
        {
            //player 1
            for (int i = 0; i < 5; i++)
                playerOneHand[i] = dc.getDeck[i];
            //player 2
            for (int i = 5; i < 10; i++)
                playerTwoHand[i - 5] = dc.getDeck[i];
            //player 3
            for (int i = 10; i < 15; i++)
                playerThreeHand[i - 10] = dc.getDeck[i];
        }

        public void sortCards()
        {
            var queryPlayerOne = from hand in playerOneHand
                              orderby hand.MyValue
                              select hand;

            var queryPlayerTwo = from hand in playerTwoHand
                                orderby hand.MyValue
                                select hand;

            var queryplayerThreeHand = from hand in playerThreeHand
                                orderby hand.MyValue
                                select hand;
            

            var index = 0;
            foreach (var element in queryPlayerOne.ToList())
            {
                sortedplayerOneHand[index] = element;
                index++;
            }

            index = 0;
            foreach (var element in queryPlayerTwo.ToList())
            {
                sortedplayerTwoHand[index] = element;
                index++;
            }

            index = 0;
            foreach (var element in queryplayerThreeHand.ToList())
            {
                sortedplayerThreeHand[index] = element;
                index++;
            }

        }

        public Bitmap SuitImageTwo(string cardSuit)
        {
            switch (cardSuit)
            {

                case "CLUBS":
                    cardsuitImage = Properties.Resources.club;
                    break;
                case "HEARTS":
                    cardsuitImage = Properties.Resources.heart;
                    break;
                case "DIAMONDS":
                    cardsuitImage = Properties.Resources.diamond;
                    break;
                case "SPADES":
                    cardsuitImage = Properties.Resources.spade;
                    break;
            }

            return cardsuitImage;
        }
        public void displayCards()
        {
            #region PlayerOne

            
            for (int playerCard = 0; playerCard < 5; playerCard++)
            {

          
                cardsuitImage = SuitImageTwo(sortedplayerOneHand[playerCard].MySuit.ToString());

                switch (playerCard)
                {
                    case 0:
                        lblPlayerOneCard1.Text = sortedplayerOneHand[playerCard].MyValue.ToString();
                        pbPlayerOneCard1.Image = cardsuitImage;
                        break;
                    case 1:
                        lblPlayerOneCard2.Text = sortedplayerOneHand[playerCard].MyValue.ToString();
                        pbPlayerOneCard2.Image = cardsuitImage;
                        break;
                    case 2:
                        lblPlayerOneCard3.Text = sortedplayerOneHand[playerCard].MyValue.ToString();
                        pbPlayerOneCard3.Image = cardsuitImage;
                        break;
                    case 3:
                        lblPlayerOneCard4.Text = sortedplayerOneHand[playerCard].MyValue.ToString();
                        pbPlayerOneCard4.Image = cardsuitImage;
                        break;
                    case 4:
                        lblPlayerOneCard5.Text = sortedplayerOneHand[playerCard].MyValue.ToString();
                        pbPlayerOneCard5.Image = cardsuitImage;
                        break;
                }
            }
            #endregion

            #region PlayerTwo
            for (int playerCard = 0; playerCard < 5; playerCard++)
            {
                cardsuitImage = SuitImageTwo(sortedplayerTwoHand[playerCard].MySuit.ToString());

                switch (playerCard)
                {
                    case 0:
                        lblPlayerTwoCard1.Text = sortedplayerTwoHand[playerCard].MyValue.ToString();
                        pbPlayerTwoCard1.Image = cardsuitImage;
                        break;
                    case 1:
                        lblPlayerTwoCard2.Text = sortedplayerTwoHand[playerCard].MyValue.ToString();
                        pbPlayerTwoCard2.Image = cardsuitImage;
                        break;
                    case 2:
                        lblPlayerTwoCard3.Text = sortedplayerTwoHand[playerCard].MyValue.ToString();
                        pbPlayerTwoCard3.Image = cardsuitImage;
                        break;
                    case 3:
                        lblPlayerTwoCard4.Text = sortedplayerTwoHand[playerCard].MyValue.ToString();
                        pbPlayerTwoCard4.Image = cardsuitImage;
                        break;
                    case 4:
                        lblPlayerTwoCard5.Text = sortedplayerTwoHand[playerCard].MyValue.ToString();
                        pbPlayerTwoCard5.Image = cardsuitImage;
                        break;
                }
            }
            #endregion

            #region playerThreeHand
            for (int playerCard = 0; playerCard < 5; playerCard++)
            {
                cardsuitImage = SuitImageTwo(sortedplayerThreeHand[playerCard].MySuit.ToString());

                switch (playerCard)
                {
                    case 0:
                        lblPlayerThreeCard1.Text = sortedplayerThreeHand[playerCard].MyValue.ToString();
                        pbPlayerThreeCard1.Image = cardsuitImage;
                        break;
                    case 1:
                        lblPlayerThreeCard2.Text = sortedplayerThreeHand[playerCard].MyValue.ToString();
                        pbPlayerThreeCard2.Image = cardsuitImage;
                        break;
                    case 2:
                        lblPlayerThreeCard3.Text = sortedplayerThreeHand[playerCard].MyValue.ToString();
                        pbPlayerThreeCard3.Image = cardsuitImage;
                        break;
                    case 3:
                        lblPlayerThreeCard4.Text = sortedplayerThreeHand[playerCard].MyValue.ToString();
                        pbPlayerThreeCard4.Image = cardsuitImage;
                        break;
                    case 4:
                        lblPlayerThreeCard5.Text = sortedplayerThreeHand[playerCard].MyValue.ToString();
                        pbPlayerThreeCard5.Image = cardsuitImage;
                        break;
                }
            }
            #endregion
        }

        public void evaluateHands()
        {
            HandEvaluator playerOneHandEvaluator = new HandEvaluator(sortedplayerOneHand);
            HandEvaluator playerTwoHandEvaluator = new HandEvaluator(sortedplayerTwoHand);
            HandEvaluator playerThreeHandHandEvaluator = new HandEvaluator(sortedplayerThreeHand);

            Hand playerOneHand = playerOneHandEvaluator.EvaluateHand();
            Hand playerTwoHand = playerTwoHandEvaluator.EvaluateHand();
            Hand playerThreeHand = playerThreeHandHandEvaluator.EvaluateHand();

            lblPlayerOneHandEvaluation.Text = playerOneHand.ToString();
            lblPlayerTwoEvaluation.Text = playerTwoHand.ToString();
            lblPlayerThreeEvaluation.Text = playerThreeHand.ToString();

            // evaluates the winner
            if (playerOneHand > playerTwoHand && playerOneHand > playerThreeHand)
            {
                lblWinner.Text = string.Format("the winner is Joe - {0}", playerOneHand);   
            }
            else if (playerTwoHand > playerOneHand && playerTwoHand > playerThreeHand)
            {
                lblWinner.Text = string.Format("the winner is Jen - {0}", playerTwoHand);   
            }
            else if (playerThreeHand > playerOneHand && playerThreeHand > playerTwoHand)
            {
                lblWinner.Text = string.Format("the winner is Bob - {0}", playerThreeHand);
            }
            else
            {
                //higher poker hand
                if (playerOneHandEvaluator.HandValues.Total > playerTwoHandEvaluator.HandValues.Total && playerOneHandEvaluator.HandValues.Total > playerThreeHandHandEvaluator.HandValues.Total)
                    lblWinner.Text = string.Format("the winner is Joe - {0}", playerOneHand);
                else if (playerTwoHandEvaluator.HandValues.Total > playerOneHandEvaluator.HandValues.Total && playerTwoHandEvaluator.HandValues.Total > playerThreeHandHandEvaluator.HandValues.Total)
                    lblWinner.Text = string.Format("the winner is Jen - {0}", playerTwoHand);
                else if (playerThreeHandHandEvaluator.HandValues.Total > playerOneHandEvaluator.HandValues.Total && playerThreeHandHandEvaluator.HandValues.Total > playerTwoHandEvaluator.HandValues.Total)
                    lblWinner.Text = string.Format("the winner is Bob - {0}", playerThreeHand);
                //both value 
                else if (playerOneHandEvaluator.HandValues.HighCard > playerTwoHandEvaluator.HandValues.HighCard && playerOneHandEvaluator.HandValues.HighCard > playerThreeHandHandEvaluator.HandValues.HighCard)
                    lblWinner.Text = string.Format("the winner is Joe - {0}", playerOneHand);
                else if (playerTwoHandEvaluator.HandValues.HighCard > playerOneHandEvaluator.HandValues.HighCard && playerTwoHandEvaluator.HandValues.HighCard > playerThreeHandHandEvaluator.HandValues.HighCard)
                    lblWinner.Text = string.Format("the winner is Jen - {0}", playerTwoHand);
                else if (playerThreeHandHandEvaluator.HandValues.HighCard > playerOneHandEvaluator.HandValues.HighCard || playerThreeHandHandEvaluator.HandValues.HighCard > playerTwoHandEvaluator.HandValues.HighCard)
                    lblWinner.Text = string.Format("the winner is Bob - {0}", playerThreeHand);
                else
                    lblWinner.Text = string.Format("DRAW");
            }

        }

    }
}
