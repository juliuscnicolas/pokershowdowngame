﻿namespace PokerHandShowdownWinApp
{
    partial class PokerTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.pbPlayerOneCard1 = new System.Windows.Forms.PictureBox();
            this.pbPlayerOneCard2 = new System.Windows.Forms.PictureBox();
            this.pbPlayerOneCard3 = new System.Windows.Forms.PictureBox();
            this.pbPlayerOneCard4 = new System.Windows.Forms.PictureBox();
            this.pbPlayerOneCard5 = new System.Windows.Forms.PictureBox();
            this.pbPlayerTwoCard1 = new System.Windows.Forms.PictureBox();
            this.pbPlayerTwoCard2 = new System.Windows.Forms.PictureBox();
            this.pbPlayerTwoCard5 = new System.Windows.Forms.PictureBox();
            this.pbPlayerTwoCard4 = new System.Windows.Forms.PictureBox();
            this.pbPlayerTwoCard3 = new System.Windows.Forms.PictureBox();
            this.pbPlayerThreeCard3 = new System.Windows.Forms.PictureBox();
            this.pbPlayerThreeCard2 = new System.Windows.Forms.PictureBox();
            this.pbPlayerThreeCard1 = new System.Windows.Forms.PictureBox();
            this.pbPlayerThreeCard5 = new System.Windows.Forms.PictureBox();
            this.pbPlayerThreeCard4 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPlayerOneCard1 = new System.Windows.Forms.Label();
            this.lblPlayerOneCard2 = new System.Windows.Forms.Label();
            this.lblPlayerOneCard3 = new System.Windows.Forms.Label();
            this.lblPlayerOneCard4 = new System.Windows.Forms.Label();
            this.lblPlayerOneCard5 = new System.Windows.Forms.Label();
            this.lblPlayerTwoCard5 = new System.Windows.Forms.Label();
            this.lblPlayerTwoCard4 = new System.Windows.Forms.Label();
            this.lblPlayerTwoCard3 = new System.Windows.Forms.Label();
            this.lblPlayerTwoCard2 = new System.Windows.Forms.Label();
            this.lblPlayerTwoCard1 = new System.Windows.Forms.Label();
            this.lblPlayerThreeCard5 = new System.Windows.Forms.Label();
            this.lblPlayerThreeCard4 = new System.Windows.Forms.Label();
            this.lblPlayerThreeCard3 = new System.Windows.Forms.Label();
            this.lblPlayerThreeCard2 = new System.Windows.Forms.Label();
            this.lblPlayerThreeCard1 = new System.Windows.Forms.Label();
            this.lblPlayerOneHandEvaluation = new System.Windows.Forms.Label();
            this.lblPlayerTwoEvaluation = new System.Windows.Forms.Label();
            this.lblPlayerThreeEvaluation = new System.Windows.Forms.Label();
            this.lblWinner = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerOneCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerOneCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerOneCard3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerOneCard4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerOneCard5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerTwoCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerTwoCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerTwoCard5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerTwoCard4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerTwoCard3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerThreeCard3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerThreeCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerThreeCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerThreeCard5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerThreeCard4)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(147, 378);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(419, 48);
            this.button1.TabIndex = 0;
            this.button1.Text = "Play";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pbPlayerOneCard1
            // 
            this.pbPlayerOneCard1.Location = new System.Drawing.Point(147, 27);
            this.pbPlayerOneCard1.Name = "pbPlayerOneCard1";
            this.pbPlayerOneCard1.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerOneCard1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerOneCard1.TabIndex = 1;
            this.pbPlayerOneCard1.TabStop = false;
            // 
            // pbPlayerOneCard2
            // 
            this.pbPlayerOneCard2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbPlayerOneCard2.Location = new System.Drawing.Point(232, 27);
            this.pbPlayerOneCard2.Name = "pbPlayerOneCard2";
            this.pbPlayerOneCard2.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerOneCard2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerOneCard2.TabIndex = 2;
            this.pbPlayerOneCard2.TabStop = false;
            // 
            // pbPlayerOneCard3
            // 
            this.pbPlayerOneCard3.Location = new System.Drawing.Point(317, 27);
            this.pbPlayerOneCard3.Name = "pbPlayerOneCard3";
            this.pbPlayerOneCard3.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerOneCard3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerOneCard3.TabIndex = 3;
            this.pbPlayerOneCard3.TabStop = false;
            // 
            // pbPlayerOneCard4
            // 
            this.pbPlayerOneCard4.Location = new System.Drawing.Point(402, 27);
            this.pbPlayerOneCard4.Name = "pbPlayerOneCard4";
            this.pbPlayerOneCard4.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerOneCard4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerOneCard4.TabIndex = 4;
            this.pbPlayerOneCard4.TabStop = false;
            // 
            // pbPlayerOneCard5
            // 
            this.pbPlayerOneCard5.Location = new System.Drawing.Point(488, 27);
            this.pbPlayerOneCard5.Name = "pbPlayerOneCard5";
            this.pbPlayerOneCard5.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerOneCard5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerOneCard5.TabIndex = 5;
            this.pbPlayerOneCard5.TabStop = false;
            // 
            // pbPlayerTwoCard1
            // 
            this.pbPlayerTwoCard1.Location = new System.Drawing.Point(146, 151);
            this.pbPlayerTwoCard1.Name = "pbPlayerTwoCard1";
            this.pbPlayerTwoCard1.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerTwoCard1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerTwoCard1.TabIndex = 6;
            this.pbPlayerTwoCard1.TabStop = false;
            // 
            // pbPlayerTwoCard2
            // 
            this.pbPlayerTwoCard2.Location = new System.Drawing.Point(231, 151);
            this.pbPlayerTwoCard2.Name = "pbPlayerTwoCard2";
            this.pbPlayerTwoCard2.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerTwoCard2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerTwoCard2.TabIndex = 7;
            this.pbPlayerTwoCard2.TabStop = false;
            // 
            // pbPlayerTwoCard5
            // 
            this.pbPlayerTwoCard5.Location = new System.Drawing.Point(487, 151);
            this.pbPlayerTwoCard5.Name = "pbPlayerTwoCard5";
            this.pbPlayerTwoCard5.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerTwoCard5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerTwoCard5.TabIndex = 10;
            this.pbPlayerTwoCard5.TabStop = false;
            // 
            // pbPlayerTwoCard4
            // 
            this.pbPlayerTwoCard4.Location = new System.Drawing.Point(401, 151);
            this.pbPlayerTwoCard4.Name = "pbPlayerTwoCard4";
            this.pbPlayerTwoCard4.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerTwoCard4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerTwoCard4.TabIndex = 9;
            this.pbPlayerTwoCard4.TabStop = false;
            // 
            // pbPlayerTwoCard3
            // 
            this.pbPlayerTwoCard3.Location = new System.Drawing.Point(316, 151);
            this.pbPlayerTwoCard3.Name = "pbPlayerTwoCard3";
            this.pbPlayerTwoCard3.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerTwoCard3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerTwoCard3.TabIndex = 8;
            this.pbPlayerTwoCard3.TabStop = false;
            // 
            // pbPlayerThreeCard3
            // 
            this.pbPlayerThreeCard3.Location = new System.Drawing.Point(317, 271);
            this.pbPlayerThreeCard3.Name = "pbPlayerThreeCard3";
            this.pbPlayerThreeCard3.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerThreeCard3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerThreeCard3.TabIndex = 13;
            this.pbPlayerThreeCard3.TabStop = false;
            // 
            // pbPlayerThreeCard2
            // 
            this.pbPlayerThreeCard2.Location = new System.Drawing.Point(232, 271);
            this.pbPlayerThreeCard2.Name = "pbPlayerThreeCard2";
            this.pbPlayerThreeCard2.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerThreeCard2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerThreeCard2.TabIndex = 12;
            this.pbPlayerThreeCard2.TabStop = false;
            // 
            // pbPlayerThreeCard1
            // 
            this.pbPlayerThreeCard1.Location = new System.Drawing.Point(147, 271);
            this.pbPlayerThreeCard1.Name = "pbPlayerThreeCard1";
            this.pbPlayerThreeCard1.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerThreeCard1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerThreeCard1.TabIndex = 11;
            this.pbPlayerThreeCard1.TabStop = false;
            // 
            // pbPlayerThreeCard5
            // 
            this.pbPlayerThreeCard5.Location = new System.Drawing.Point(488, 271);
            this.pbPlayerThreeCard5.Name = "pbPlayerThreeCard5";
            this.pbPlayerThreeCard5.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerThreeCard5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerThreeCard5.TabIndex = 15;
            this.pbPlayerThreeCard5.TabStop = false;
            // 
            // pbPlayerThreeCard4
            // 
            this.pbPlayerThreeCard4.Location = new System.Drawing.Point(402, 271);
            this.pbPlayerThreeCard4.Name = "pbPlayerThreeCard4";
            this.pbPlayerThreeCard4.Size = new System.Drawing.Size(78, 90);
            this.pbPlayerThreeCard4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPlayerThreeCard4.TabIndex = 14;
            this.pbPlayerThreeCard4.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "JOE :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 193);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "JEN:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 311);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "BOB:";
            // 
            // lblPlayerOneCard1
            // 
            this.lblPlayerOneCard1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerOneCard1.Location = new System.Drawing.Point(143, 9);
            this.lblPlayerOneCard1.Name = "lblPlayerOneCard1";
            this.lblPlayerOneCard1.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerOneCard1.TabIndex = 19;
            this.lblPlayerOneCard1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerOneCard2
            // 
            this.lblPlayerOneCard2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerOneCard2.Location = new System.Drawing.Point(232, 9);
            this.lblPlayerOneCard2.Name = "lblPlayerOneCard2";
            this.lblPlayerOneCard2.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerOneCard2.TabIndex = 20;
            this.lblPlayerOneCard2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerOneCard3
            // 
            this.lblPlayerOneCard3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerOneCard3.Location = new System.Drawing.Point(314, 9);
            this.lblPlayerOneCard3.Name = "lblPlayerOneCard3";
            this.lblPlayerOneCard3.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerOneCard3.TabIndex = 21;
            this.lblPlayerOneCard3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerOneCard4
            // 
            this.lblPlayerOneCard4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerOneCard4.Location = new System.Drawing.Point(399, 9);
            this.lblPlayerOneCard4.Name = "lblPlayerOneCard4";
            this.lblPlayerOneCard4.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerOneCard4.TabIndex = 22;
            this.lblPlayerOneCard4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerOneCard5
            // 
            this.lblPlayerOneCard5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerOneCard5.Location = new System.Drawing.Point(484, 9);
            this.lblPlayerOneCard5.Name = "lblPlayerOneCard5";
            this.lblPlayerOneCard5.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerOneCard5.TabIndex = 23;
            this.lblPlayerOneCard5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerTwoCard5
            // 
            this.lblPlayerTwoCard5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerTwoCard5.Location = new System.Drawing.Point(484, 133);
            this.lblPlayerTwoCard5.Name = "lblPlayerTwoCard5";
            this.lblPlayerTwoCard5.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerTwoCard5.TabIndex = 28;
            this.lblPlayerTwoCard5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerTwoCard4
            // 
            this.lblPlayerTwoCard4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerTwoCard4.Location = new System.Drawing.Point(399, 133);
            this.lblPlayerTwoCard4.Name = "lblPlayerTwoCard4";
            this.lblPlayerTwoCard4.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerTwoCard4.TabIndex = 27;
            this.lblPlayerTwoCard4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerTwoCard3
            // 
            this.lblPlayerTwoCard3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerTwoCard3.Location = new System.Drawing.Point(314, 133);
            this.lblPlayerTwoCard3.Name = "lblPlayerTwoCard3";
            this.lblPlayerTwoCard3.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerTwoCard3.TabIndex = 26;
            this.lblPlayerTwoCard3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerTwoCard2
            // 
            this.lblPlayerTwoCard2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerTwoCard2.Location = new System.Drawing.Point(232, 133);
            this.lblPlayerTwoCard2.Name = "lblPlayerTwoCard2";
            this.lblPlayerTwoCard2.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerTwoCard2.TabIndex = 25;
            this.lblPlayerTwoCard2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerTwoCard1
            // 
            this.lblPlayerTwoCard1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerTwoCard1.Location = new System.Drawing.Point(143, 133);
            this.lblPlayerTwoCard1.Name = "lblPlayerTwoCard1";
            this.lblPlayerTwoCard1.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerTwoCard1.TabIndex = 24;
            this.lblPlayerTwoCard1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerThreeCard5
            // 
            this.lblPlayerThreeCard5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerThreeCard5.Location = new System.Drawing.Point(484, 253);
            this.lblPlayerThreeCard5.Name = "lblPlayerThreeCard5";
            this.lblPlayerThreeCard5.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerThreeCard5.TabIndex = 33;
            this.lblPlayerThreeCard5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerThreeCard4
            // 
            this.lblPlayerThreeCard4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerThreeCard4.Location = new System.Drawing.Point(399, 253);
            this.lblPlayerThreeCard4.Name = "lblPlayerThreeCard4";
            this.lblPlayerThreeCard4.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerThreeCard4.TabIndex = 32;
            this.lblPlayerThreeCard4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerThreeCard3
            // 
            this.lblPlayerThreeCard3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerThreeCard3.Location = new System.Drawing.Point(314, 253);
            this.lblPlayerThreeCard3.Name = "lblPlayerThreeCard3";
            this.lblPlayerThreeCard3.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerThreeCard3.TabIndex = 31;
            this.lblPlayerThreeCard3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerThreeCard2
            // 
            this.lblPlayerThreeCard2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerThreeCard2.Location = new System.Drawing.Point(232, 253);
            this.lblPlayerThreeCard2.Name = "lblPlayerThreeCard2";
            this.lblPlayerThreeCard2.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerThreeCard2.TabIndex = 30;
            this.lblPlayerThreeCard2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerThreeCard1
            // 
            this.lblPlayerThreeCard1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerThreeCard1.Location = new System.Drawing.Point(143, 253);
            this.lblPlayerThreeCard1.Name = "lblPlayerThreeCard1";
            this.lblPlayerThreeCard1.Size = new System.Drawing.Size(82, 15);
            this.lblPlayerThreeCard1.TabIndex = 29;
            this.lblPlayerThreeCard1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayerOneHandEvaluation
            // 
            this.lblPlayerOneHandEvaluation.Location = new System.Drawing.Point(607, 68);
            this.lblPlayerOneHandEvaluation.Name = "lblPlayerOneHandEvaluation";
            this.lblPlayerOneHandEvaluation.Size = new System.Drawing.Size(228, 13);
            this.lblPlayerOneHandEvaluation.TabIndex = 34;
            // 
            // lblPlayerTwoEvaluation
            // 
            this.lblPlayerTwoEvaluation.Location = new System.Drawing.Point(607, 193);
            this.lblPlayerTwoEvaluation.Name = "lblPlayerTwoEvaluation";
            this.lblPlayerTwoEvaluation.Size = new System.Drawing.Size(228, 22);
            this.lblPlayerTwoEvaluation.TabIndex = 35;
            // 
            // lblPlayerThreeEvaluation
            // 
            this.lblPlayerThreeEvaluation.Location = new System.Drawing.Point(607, 311);
            this.lblPlayerThreeEvaluation.Name = "lblPlayerThreeEvaluation";
            this.lblPlayerThreeEvaluation.Size = new System.Drawing.Size(228, 13);
            this.lblPlayerThreeEvaluation.TabIndex = 36;
            // 
            // lblWinner
            // 
            this.lblWinner.Location = new System.Drawing.Point(607, 396);
            this.lblWinner.Name = "lblWinner";
            this.lblWinner.Size = new System.Drawing.Size(228, 13);
            this.lblWinner.TabIndex = 37;
            // 
            // PokerTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 468);
            this.Controls.Add(this.lblWinner);
            this.Controls.Add(this.lblPlayerThreeEvaluation);
            this.Controls.Add(this.lblPlayerTwoEvaluation);
            this.Controls.Add(this.lblPlayerOneHandEvaluation);
            this.Controls.Add(this.lblPlayerThreeCard5);
            this.Controls.Add(this.lblPlayerThreeCard4);
            this.Controls.Add(this.lblPlayerThreeCard3);
            this.Controls.Add(this.lblPlayerThreeCard2);
            this.Controls.Add(this.lblPlayerThreeCard1);
            this.Controls.Add(this.lblPlayerTwoCard5);
            this.Controls.Add(this.lblPlayerTwoCard4);
            this.Controls.Add(this.lblPlayerTwoCard3);
            this.Controls.Add(this.lblPlayerTwoCard2);
            this.Controls.Add(this.lblPlayerTwoCard1);
            this.Controls.Add(this.lblPlayerOneCard5);
            this.Controls.Add(this.lblPlayerOneCard4);
            this.Controls.Add(this.lblPlayerOneCard3);
            this.Controls.Add(this.lblPlayerOneCard2);
            this.Controls.Add(this.lblPlayerOneCard1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbPlayerThreeCard5);
            this.Controls.Add(this.pbPlayerThreeCard4);
            this.Controls.Add(this.pbPlayerThreeCard3);
            this.Controls.Add(this.pbPlayerThreeCard2);
            this.Controls.Add(this.pbPlayerThreeCard1);
            this.Controls.Add(this.pbPlayerTwoCard5);
            this.Controls.Add(this.pbPlayerTwoCard4);
            this.Controls.Add(this.pbPlayerTwoCard3);
            this.Controls.Add(this.pbPlayerTwoCard2);
            this.Controls.Add(this.pbPlayerTwoCard1);
            this.Controls.Add(this.pbPlayerOneCard5);
            this.Controls.Add(this.pbPlayerOneCard4);
            this.Controls.Add(this.pbPlayerOneCard3);
            this.Controls.Add(this.pbPlayerOneCard2);
            this.Controls.Add(this.pbPlayerOneCard1);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PokerTable";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Poker Showcase Showdown";
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerOneCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerOneCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerOneCard3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerOneCard4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerOneCard5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerTwoCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerTwoCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerTwoCard5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerTwoCard4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerTwoCard3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerThreeCard3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerThreeCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerThreeCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerThreeCard5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayerThreeCard4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pbPlayerOneCard1;
        private System.Windows.Forms.PictureBox pbPlayerOneCard2;
        private System.Windows.Forms.PictureBox pbPlayerOneCard3;
        private System.Windows.Forms.PictureBox pbPlayerOneCard4;
        private System.Windows.Forms.PictureBox pbPlayerOneCard5;
        private System.Windows.Forms.PictureBox pbPlayerTwoCard1;
        private System.Windows.Forms.PictureBox pbPlayerTwoCard2;
        private System.Windows.Forms.PictureBox pbPlayerTwoCard5;
        private System.Windows.Forms.PictureBox pbPlayerTwoCard4;
        private System.Windows.Forms.PictureBox pbPlayerTwoCard3;
        private System.Windows.Forms.PictureBox pbPlayerThreeCard3;
        private System.Windows.Forms.PictureBox pbPlayerThreeCard2;
        private System.Windows.Forms.PictureBox pbPlayerThreeCard1;
        private System.Windows.Forms.PictureBox pbPlayerThreeCard5;
        private System.Windows.Forms.PictureBox pbPlayerThreeCard4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblPlayerOneCard1;
        private System.Windows.Forms.Label lblPlayerOneCard2;
        private System.Windows.Forms.Label lblPlayerOneCard3;
        private System.Windows.Forms.Label lblPlayerOneCard4;
        private System.Windows.Forms.Label lblPlayerOneCard5;
        private System.Windows.Forms.Label lblPlayerTwoCard5;
        private System.Windows.Forms.Label lblPlayerTwoCard4;
        private System.Windows.Forms.Label lblPlayerTwoCard3;
        private System.Windows.Forms.Label lblPlayerTwoCard2;
        private System.Windows.Forms.Label lblPlayerTwoCard1;
        private System.Windows.Forms.Label lblPlayerThreeCard5;
        private System.Windows.Forms.Label lblPlayerThreeCard4;
        private System.Windows.Forms.Label lblPlayerThreeCard3;
        private System.Windows.Forms.Label lblPlayerThreeCard2;
        private System.Windows.Forms.Label lblPlayerThreeCard1;
        private System.Windows.Forms.Label lblPlayerOneHandEvaluation;
        private System.Windows.Forms.Label lblPlayerTwoEvaluation;
        private System.Windows.Forms.Label lblPlayerThreeEvaluation;
        private System.Windows.Forms.Label lblWinner;
    }
}

